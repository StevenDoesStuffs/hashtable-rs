pub use std::{
    mem::{MaybeUninit, self},
    hash::{
        Hash,
        Hasher,
        BuildHasher
    },
    collections::hash_map::{
        RandomState,
        HashMap as StdMap
    },
    cmp,
    fmt
};
pub use packed_simd::{u8x8 as u8_vec, i8x8 as i8_vec};
pub use lazy_static::lazy_static;

#[inline(always)]
pub fn likely(b: bool) -> bool {
    // it's actually not unsafe
    #[allow(unused_unsafe)]
    unsafe{std::intrinsics::likely(b)}
}
#[inline(always)]
pub fn unlikely(b: bool) -> bool {
    // it's actually not unsafe
    #[allow(unused_unsafe)]
    unsafe{std::intrinsics::unlikely(b)}
}
