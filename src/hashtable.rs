use crate::uses::*;

const MIN_LOG: usize = 5;
const GROUP: usize = 8;
const LOAD_LO: f64 = 0.2;
const LOAD_HI: f64 = 0.84;
lazy_static!{
    // reee >:(
    static ref DIST_SEQ: i8_vec = i8_vec::from([0,1,2,3,4,5,6,7]);
    static ref DIST_ZERO: i8_vec = i8_vec::from([0; GROUP]);
    static ref H2_ZERO: u8_vec = u8_vec::from([0; GROUP]);
}

pub struct HashMap<K: Hash, V, S: BuildHasher = RandomState> {
    mask: usize,
    items: usize,
    load_lo: f64,
    load_hi: f64,
    build_hasher: S,
    buckets: Buckets<K, V>
}

struct Buckets<K, V> {
    h2: Vec<u8>, // h2 = 0 for empty elements
    dist: Vec<i8>, // dist = 0 for empty elements
    key: Vec<MaybeUninit<K>>,
    value: Vec<MaybeUninit<V>>
}

impl<K: Hash + Eq, V, S: BuildHasher + Default> Default for HashMap<K, V, S> {
    fn default() -> Self {
        Self::init(MIN_LOG, S::default())
    }
}
impl<K: Hash + Eq, V, S: BuildHasher + Default> HashMap<K, V, S> {
    pub fn new() -> Self {
        Self::default()
    }
}

#[derive(Clone, Copy)]
struct BrokenHash {
    h2: u8,
    index: usize
}

struct Bucket<K, V> {
    key: K,
    value: V,
    h2: u8,
    dist: i8
}


impl<K, V> Buckets<K, V> {

    fn new(cap: usize) -> Self {
        fn empty_vec<T>(cap: usize) -> Vec<MaybeUninit<T>> {
            let mut v = vec![];
            v.resize_with(cap, MaybeUninit::uninit);
            v
        }
        Buckets {
            // buffer at end for SIMD
            h2: vec![0; cap + GROUP],
            dist: vec![0; cap + GROUP],
            key: empty_vec(cap),
            value: empty_vec(cap)
        }
    }

    // PRECONDITION: the dist for entry is valid
    fn set(&mut self, i: usize, entry: Bucket<K, V>) {
        if self.h2[i] != 0 { // drop values if they exist
            unsafe {
                self.key[i].read();
                self.value[i].read();
            }
        }
        self.key[i].write(entry.key);
        self.value[i].write(entry.value);
        self.h2[i] = entry.h2;
        self.dist[i] = entry.dist;
    }

    unsafe fn take(&mut self, i: usize) -> Bucket<K, V> {
        let key = self.key[i].read();
        let value = self.value[i].read();
        let h2: u8 = mem::take(&mut self.h2[i]);
        let dist = mem::take(&mut self.dist[i]);
        Bucket {key, value, h2, dist}
    }

    // SAFETY: assumes i is a valid index
    // PRECONDITION: the dist for entry is valid
    unsafe fn replace(&mut self, i: usize, entry: Bucket<K, V>) -> Bucket<K, V> {
        Bucket {
            key: mem::replace(self.key[i].get_mut(), entry.key),
            value: mem::replace(self.value[i].get_mut(), entry.value),
            h2: mem::replace(&mut self.h2[i], entry.h2),
            dist: mem::replace(&mut self.dist[i], entry.dist)
        }
    }

    // NOTE: automatically calcuates new dist
    // PRECONDITION: neither swap to before their hash index
    fn swap_indices(&mut self, i1: usize, i2: usize) {
        if i1 == i2 {return}

        let mask = self.key.len() - 1;
        let h1 = (i1 - self.dist[i1] as usize) & mask;
        let h2 = (i2 - self.dist[i2] as usize) & mask;
        if self.h2[i1] != 0 {
            self.dist[i1] = ((i2 - h1) & mask) as i8;
        }
        if self.h2[i2] != 0 {
            self.dist[i2] = ((i1 - h2) & mask) as i8;
        }

        self.key.swap(i1, i2);
        self.value.swap(i1, i2);
        self.h2.swap(i1, i2);
        self.dist.swap(i1, i2);
    }
}

impl<K: Hash + Eq, V, S: BuildHasher> HashMap<K, V, S> {

    // util methods

    fn init(log: usize, build_hasher: S) -> Self {
        assert!(log >= MIN_LOG);
        let cap = 1 << log;
        Self {
            mask: cap - 1,
            items: 0,
            load_lo: LOAD_LO,
            load_hi: LOAD_HI,
            build_hasher,
            buckets: Buckets::new(cap)
        }
    }

    #[inline]
    fn hash(&self, key: &K) -> BrokenHash {
        let mut hasher = self.build_hasher.build_hasher();
        key.hash(&mut hasher);
        let hash = hasher.finish();
        let mut h2 = (hash >> 56) as u8;
        if h2 == 0 {h2 += 1} // never hash h2 to 0
        BrokenHash {
            h2,
            index: (hash as usize) & self.mask
        }
    }

    #[inline]
    fn update_end(&mut self) {
        let cap = self.mask + 1;
        fn copy_to_end<T: Copy>(slice: &mut [T], cap: usize) {
            let (start, end) = slice.split_at_mut(cap);
            end.copy_from_slice(&start[0..GROUP]);
        }
        copy_to_end(&mut self.buckets.h2, cap);
        copy_to_end(&mut self.buckets.dist, cap);
    }


    // find methods
    #[inline]
    fn find_range(&self, hash: &BrokenHash) -> usize {
        let mut num_groups = 1;
        // the vast majority of the time we're gonna end up with one group
        while unlikely(self.buckets.dist[
            (hash.index + GROUP*num_groups) & self.mask
        ] as usize >= GROUP*num_groups) {
            num_groups += 1;
        }
        num_groups
    }

    #[inline]
    fn find(&self, key: &K, hash: &BrokenHash, num_groups: usize) -> Option<usize> {
        // GROUP elements at a time yeet
        let cmp = u8_vec::from([hash.h2; GROUP]);
        for group in (0..num_groups).rev() {
            let start = (hash.index + group*GROUP) & self.mask;
            let v = u8_vec::from_slice_unaligned(&self.buckets.h2[start..(start + GROUP)]);
            let mut bitmask = v.eq(cmp).bitmask();

            while bitmask != 0 {
                let i = (start + bitmask.trailing_zeros() as usize) & self.mask;
                // SAFETY: h2[i] != 0
                if likely(self.buckets.dist[i] as usize == (i - hash.index) & self.mask &&
                        unsafe {self.buckets.key[i].get_ref() == key}) {

                    return Some(i);
                }
                bitmask &= bitmask - 1; // erases the LSB
            }
        }

        None
    }

    #[inline]
    fn find_key(&self, key: &K) -> Option<usize> {
        let hash = self.hash(&key);
        let num_groups = self.find_range(&hash);
        Some(self.find(&key, &hash, num_groups)?)
    }

    // returns Ok(index) if item is present, Err(next_index) if not
    #[inline]
    fn find_next(&self, hash: &BrokenHash, num_groups: usize) -> usize {
        if self.buckets.h2[hash.index] == 0 {return hash.index}

        // insertion point guaranteed to be in last group
        let offset = (num_groups - 1) * GROUP;
        let start = (hash.index + offset) & self.mask;
        let v = i8_vec::from_slice_unaligned(
            &self.buckets.dist[start..(start + GROUP)]
        ) - (*DIST_SEQ + offset as i8);
        let bitmask = v.lt(*DIST_ZERO).bitmask();
        let dist = if bitmask == 0 {
            GROUP
        } else {
            bitmask.trailing_zeros() as usize
        };

        (start + dist) & self.mask
    }


    // public methods

    pub fn insert(&mut self, key: K, value: V) -> Option<&V> {

        let (mut entry, mut i, mut dist) = {
            let hash = self.hash(&key);
            let num_groups = self.find_range(&hash);
            if let Some(index) = self.find(&key, &hash, num_groups) {
                // SAFETY: find() = Ok
                return unsafe {Some(self.buckets.value[index].get_ref())}
            }
            let i = self.find_next(&hash, num_groups);
            let dist = ((i - hash.index) & self.mask) as i8;
            (Some(Bucket {key, value, h2: hash.h2, dist}), i, dist)
        };
        while self.buckets.h2[i] != 0 {
            // swap if greater
            if dist > self.buckets.dist[i] {
                let temp = self.buckets.dist[i];
                unsafe { // SAFETY: h2 != 0, so i is valid
                    let mut old_entry = entry.unwrap();
                    old_entry.dist = dist;
                    entry = Some(self.buckets.replace(i, old_entry));
                }
                dist = temp;
            }
            // move on
            i = (i + 1) & self.mask;
            dist += 1;
        }
        // set after we have empty slot
        let mut entry = entry.unwrap();
        entry.dist = dist;
        self.buckets.set(i, entry);

        self.update_end();
        self.items += 1;

        if self.items as f64 / (self.mask as f64 + 1.0) > self.load_hi {
            self.double();
        }

        None
    }

    pub fn remove(&mut self, key: &K) -> Option<V> {
        // last = thing we wanna replace
        let (value, mut last) = {
            let hash = self.hash(&key);
            let num_groups = self.find_range(&hash);
            // in the middle of a segment
            let i = self.find(&key, &hash, num_groups)?;
            // end of segment, take goes after as it modifies dist
            let last = (self.find_next(&hash, num_groups) - 1) & self.mask;
            // SAFETY: find() = Ok
            let value = unsafe {self.buckets.take(i).value};
            // swap middle with end of segment
            self.buckets.swap_indices(i, last);
            (value, last)
        };
        let mut i = (last + 1) & self.mask;
        while self.buckets.h2[i] != 0 && self.buckets.dist[i] != 0 {
            let next = (i + 1) & self.mask;
            // if we're at the end of a segment
            if self.buckets.dist[next] != self.buckets.dist[i] + 1 || self.buckets.h2[next] == 0 {
                // replace last
                self.buckets.swap_indices(last, i);
                last = i;
            }
            i = next;
        }
        self.update_end();
        self.items -= 1;

        // TODO: RESIZE

        Some(value)
    }

    pub fn get(&self, key: &K) -> Option<&V> {
        unsafe { // SAFETY: find() = Ok
            Some(self.buckets.value[self.find_key(&key)?].get_ref())
        }
    }
    pub fn get_mut(&mut self, key: &K) -> Option<&mut V> {
        unsafe { // SAFETY: find() = Ok
            let index = self.find_key(&key)?;
            Some(self.buckets.value[index].get_mut())
        }
    }

    pub fn clear(&mut self) {
        for i in 0..self.buckets.h2.len() {
            self.buckets.h2[i] = 0;
            self.buckets.dist[i] = 0;
        }
        self.items = 0;
    }


    // resize methods

    // assumes the thing isn't full, which it never should be
    #[inline]
    fn find_start(&self) -> usize {
        let mut group = 0;
        loop {
            let start = group * GROUP;
            let v = u8_vec::from_slice_unaligned(
                &self.buckets.h2[start..(start + GROUP)]
            );

            let mask = v.eq(*H2_ZERO).bitmask();

            if likely(mask != 0) {
                return start + mask.trailing_zeros() as usize;
            } else {
                group += 1;
            }
        }
    }

    fn double(&mut self) {
        // fields that need updating: mask, buckets

        let start = self.find_start();
        let end = start + (self.mask + 1);
        let mut empty_a = start;
        let mut empty_b = end;

        let iter = {
            let old_mask = self.mask;
            self.mask = (self.mask << 1) + 1; // swap mask for hashing
            (empty_a..empty_b).map(move |x| x & old_mask)
        };

        let mut buckets = Buckets::<K, V>::new(self.mask + 1);

        for i in iter {
            if self.buckets.h2[i] == 0 {
                continue;
            }

            let hash = unsafe { // SAFETY: h2[i] != 0
                self.hash(self.buckets.key[i].get_ref())
            };
            let empty = if hash.index >= start && hash.index < end {
                &mut empty_a
            } else {
                &mut empty_b
            };

            if buckets.h2[hash.index] == 0 { // move up if empty
                *empty = hash.index;
            }
            unsafe { // SAFETY: h2[i] != 0
                let mut entry = self.buckets.take(i);
                entry.dist = ((*empty - hash.index) & self.mask) as i8;
                buckets.set(*empty, entry);
            }
            *empty = (*empty + 1) & self.mask;
        }

        self.buckets = buckets;
    }
}
