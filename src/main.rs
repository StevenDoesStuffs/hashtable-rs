#![allow(dead_code, clippy::needless_range_loop)]
#![feature(maybe_uninit_extra, maybe_uninit_ref, core_intrinsics, type_name_of_val)]

mod hashtable;
mod uses;

use crate::hashtable::HashMap;
// use std::collections::HashMap;
use rand::prelude::*;
use std::time::{Instant, Duration};
use std::{
    hash::{BuildHasher, Hasher}
};

fn insert_delete() {
    let n = 100_000_000;
    let mut vec = (0..n).collect::<Vec<_>>();
    let mut rng = SmallRng::from_entropy();
    let mut set = HashMap::<usize, ()>::new();
    vec.shuffle(&mut rng);

 
    let start = Instant::now();
    for i in 0..n {
        set.insert(vec[i], ());
    }
    let mut total = start.elapsed();

    vec.shuffle(&mut rng);

    let start = Instant::now();
    set.clear();
    for i in 0..n {
        set.insert(vec[i], ());
    }
    for i in 0..n {
        set.remove(&vec[i]);
    }
    total += start.elapsed();
    println!("{}", total.as_secs_f64());
}

fn insert_get() {
    let n = 50_000_000;
    let mut vec = vec![0; n];
    let mut rng = SmallRng::from_entropy();
    let mut total = Duration::new(0, 0);
    let mut set = HashMap::<usize, ()>::new();
    for &ratio in &[0.05, 0.25, 0.50, 100.0] {
        for i in 0..n {
            vec[i] = rng.gen_range(0, (ratio * n as f64) as usize)
        }
        let start = Instant::now();
        for i in 0..n {
            set.insert(vec[i], ());
        }
        total += start.elapsed();
    }
    println!("{}", total.as_secs_f64());
}

fn just_get() {
    let n = 50_000_000;
    let mut total = Duration::new(0, 0);
    let mut rng = SmallRng::from_entropy();
    let mut set = HashMap::<usize, ()>::new();
    for &ratio in &[0.05, 0.25, 0.50, 1.0] {
        for _ in 0..((ratio * n as f64) as usize) {
            set.insert(rng.gen_range(0, n), ());
        }
        let start = Instant::now();
        for i in 0..n {
            set.get(&i);
        }
        total += start.elapsed();
    }
    println!("{}", total.as_secs_f64());
}

fn main() {
    let mut t = 0.0;
    for _ in 0..10 {
        let n = 10_000_000;
        let mut rng = thread_rng();
    
        let mut vec = vec![0; n];
        for i in 0..n {
            vec[i] = rng.gen();
        }
    
        let mut set = HashMap::<usize, (), BuildIdentHasher>::default();
        let start = Instant::now();
        for i in 0..(n / 4) {
            for j in 0..4 {
                set.insert(vec[i*4 + j], ());
            }
            set.remove(&vec[i]);
        }
        let mut total = start.elapsed();
    
        vec.shuffle(&mut rng);
    
        let start = Instant::now();
        for i in 0..n {
            set.remove(&vec[i]);
        }
        total += start.elapsed();
        t += total.as_secs_f64();
        println!("{}", total.as_secs_f64());
    }
    println!("avg: {}", t / 10.0);
}

#[derive(Copy, Clone)]
struct IdentHasher(u64);
impl Hasher for IdentHasher {
    fn finish(&self) -> u64 {
        self.0 as u64
    }
    fn write(&mut self, data: &[u8]) {
        self.0 = u64::from_ne_bytes([data[0], data[1], data[2], data[3],
            data[4], data[5], data[6], data[7]]);
    }
}

#[derive(Default, Copy, Clone)]
struct BuildIdentHasher;
impl BuildHasher for BuildIdentHasher {
    type Hasher = IdentHasher;
    fn build_hasher(&self) -> IdentHasher {
        IdentHasher(0)
    }
}
